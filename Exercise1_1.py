import numpy as np

x = np.arange(1, 11)
a1 = np.array(x, 'i')
a2 = np.array(x, 'f')

print(x)
print(a1)
print(a2)
print(x.dtype)
print(a1.dtype)
print(a2.dtype)
