import numpy as np

M = input('Enter width: ')
N = input('Enter height: ')
P = input('Enter depth: ')
arr = np.random.randint(-100, 100, [M, N, P])
print(arr)
print(arr.max(axis=0))
print(arr.sum(axis=0))
print(arr.max(axis=1))
print(arr.sum(axis=1))
print(arr.max(axis=2))
print(arr.sum(axis=2))