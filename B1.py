import numpy as np

# data = np.genfromtxt('iris.data', delimiter=',', dtype=(float, float, float, float, "|S10"))
data = np.genfromtxt('iris.data', delimiter=',', dtype='unicode')

for i in data:
    if i[4] == 'Iris-setosa':
        i[4] = 1
    elif i[4] == 'Iris-versicolor':
        i[4] = 2
    else:
        i[4] = 3

data = data.astype(float)

setosa_data = []
versi_data = []
virginica_data = []

setosa_count = 0
versi_count = 0
virginica_count = 0

for index, i in enumerate(data[:, 4]):
    if i == 1:
        setosa_data.append(data[index])
        setosa_count = setosa_count + 1
    elif i == 2:
        versi_data.append(data[index])
        versi_count = versi_count + 1
    else:
        virginica_data.append(data[index])
        virginica_count = virginica_count + 1

setosa_data = np.reshape(setosa_data, [setosa_count, 5])
versi_data = np.reshape(versi_data, [versi_count, 5])
virginica_data = np.reshape(virginica_data, [virginica_count, 5])

setosa_sepal = [setosa_data[:,0], setosa_data[:,1]]
setosa_sepal = np.reshape(setosa_sepal, [2, len(setosa_data)])
print(np.cov(setosa_sepal))
versi_sepal = [versi_data[:,0], versi_data[:,1]]
versi_sepal = np.reshape(versi_sepal, [2, len(versi_data)])
print(np.cov(versi_sepal))
virginica_sepal = [virginica_data[:,0], virginica_data[:,1]]
virginica_sepal = np.reshape(virginica_sepal, [2, len(virginica_data)])
print(np.cov(virginica_sepal))