import numpy as np

a = np.array([np.arange(4), np.arange(10, 14)])
b = np.array([2, -1, 1, 0])

print(a)
print(b)

print(a.shape)
print(b.shape)
print(a*b)

b1 = b * 100
print(b1)
b2 = b * 100.0
print(b2)
print(b1.dtype)
print(b2.dtype)
print(b1 == b2)

arr = np.arange(10)
print(arr)
print(arr < 3)
print(np.less(arr, 3))
condition = np.logical_or(arr < 3, arr > 8)
print(condition)
new_arr = np.where(condition, arr*5, arr*-5)
print(new_arr)

import numpy.ma as MA
marr = MA.masked_array(range(10), fill_value=-999)
print(marr, marr.fill_value)
marr[2] = MA.masked
print(marr)
print(marr.mask)
narr = MA.masked_where(arr > 6, marr)
print(narr)
x = MA.filled(narr)
print(x)
print(type(x))

m1 = MA.masked_array(range(1, 9))
print(m1)
m2 = np.reshape(m1, [2, 4])
print(m2)
m3 = MA.masked_where(m2 > 6, m2)
print(m3)
print(m3*100)
print(m3*100 - np.ones([2, 4]))