import numpy as np

arr = np.array([np.arange(4), np.arange(10, 14)])
print(arr)
print(arr.shape)
print(arr.size)
print(arr.max())
print(arr.min())

print(arr.reshape([2, 2, 2]))
print(arr.transpose())
print(np.ravel(arr))
print(arr.astype('f'))