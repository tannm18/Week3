import numpy as np

x = np.zeros((2, 3, 4))
y = np.ones((2, 3, 4))
z = np.arange(1000)

print(x.shape)
print(y.shape)
print(z.shape)
print(len(x))
print(len(y))
print(len(z))