import numpy as np

x = input('Enter an integer: ')
arr = np.random.randint(0, 10, [x, x, x])
print(arr)
print(arr.max(axis=0))
print(arr.sum(axis=0))
print(arr.max(axis=1))
print(arr.sum(axis=1))
print(arr.max(axis=2))
print(arr.sum(axis=2))