import numpy as np

arr = np.random.uniform(-10, 10, [10, 8])
# print(arr)

# x = input('Enter a number: ')
x = 6.5
print(arr.flat[np.abs(arr-x).argmin()])

arr_1D = arr.flatten() - x

np.sort(arr_1D)

min_1 = arr_1D.flat[np.abs(arr_1D).argmin()] + x
index = np.abs(arr_1D).argmin()
print(min_1)

arr_1D = np.delete(arr_1D, [index])
min_2 = arr_1D.flat[np.abs(arr_1D).argmin()] + x
index = np.abs(arr_1D).argmin()
print(min_2)

arr_1D = np.delete(arr_1D, [index])
# print(arr_1D.shape)
min_3 = arr_1D.flat[np.abs(arr_1D).argmin()] + x
index = np.abs(arr_1D).argmin()
print(min_3)